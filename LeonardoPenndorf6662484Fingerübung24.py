import os
import spacy
import pydot
import xml.etree.ElementTree as ET
from collections import Counter
from matplotlib import pyplot as plt
from graphviz import Graph



#Name: Leonardo Penndorf
#Matrikelnummer: 6662484


def findNodesEdgesMetalinks(root):
    n = len(list(root[1]))
    i = 0
    entities = ["PLACE", "LOCATION", "SPATIAL_ENTITY", "NONMOTION_EVENT", "PATH"]
    links = ["OLINK", "QSLINK"]
    nodes = []
    edges = []
    metalinks = []


    # find entities
    while i < n:
        tag = root[1][i].tag
        if tag in entities:
            entId = root[1][i].attrib['id']
            entText = root[1][i].attrib['text']
            nodes.append((tag, entId, entText))
            
        i += 1

    # find links
    i = 0
    while i < n:
        tag = root[1][i].tag
        if tag in links:
            edgeId = root[1][i].attrib['id']
            relType = root[1][i].attrib['relType']
            fromId = root[1][i].attrib['fromID']
            toId = root[1][i].attrib['toID']

            edges.append((tag, edgeId, relType, fromId, toId))

        i += 1


    # find metalinks
    i = 0
    while i < n:
        tag = root[1][i].tag
        if tag == "METALINK":
            metaId = root[1][i].attrib['id']
            fromId = root[1][i].attrib['fromID']
            toId = root[1][i].attrib['toID']
            metalinks.append((metaId, fromId, toId))

        i += 1

    mergeNodes(nodes, edges, metalinks)
        
        
def mergeNodes(nodes, edges, metalinks):

    # create idList for mergedNodes
    idList = []
    for meta in metalinks:
        m1 = meta[1]
        m2 = meta[2]
        i = 0
        c = 0
        while i < len(idList):
            if m1 in idList[i]:
                idList[i].append(m2)
                c = 1
                break
            if m2 in idList[i]:
                idList[i].append(m1)
                c = 1
                break

            i += 1

        if c == 0:
            idList.append([m1, m2])
            
    # create list of mergedNodes
    mergedNodes = []
    for x in idList:
        text = []
        tag = ""
        for y in x:
            i = 0
            while i < len(nodes):
                if y == nodes[i][1]:
                    text.append(nodes[i][2])
                    tag = nodes[i][0]
                i += 1
        text = set(text)
        strText = ", ".join(text)
        strx =  ", ".join(x)
        mergedNodes.append([tag, strx, strText])

    createGraph(mergedNodes, edges, metalinks)


def createGraph(mergedNodes, edges, metalinks):

    #create Graph
    name = input("Wie soll der graph heißen?")
    name += ".gv"
    G = Graph('G', filename=name)

    # creare nodes
    for node in mergedNodes:
        if node[0] == "PLACE":
            G.attr('node', style="filled", color="deepskyblue")
            G.node(node[1], label=node[2])
        if node[0] == "LOCATION":
            G.attr('node', style="filled", color="firebrick2")
            G.node(node[1], label=node[2])
        if node[0] == "SPATIAL_ENTITY":
            G.attr('node', style="filled", color="lightgreen")
            G.node(node[1], label=node[2])
        if node[0] == "NONMOTION_EVENT":
            G.attr('node', style="filled", color="gold1")
            G.node(node[1], label=node[2])
        if node[0] == "PATH":
            G.attr('node', style="filled", color="hotpink")
            G.node(node[1], label=node[2])

    # create edges
    for edge in edges:
        n1 = ""
        n2 = ""
        for node in mergedNodes:
            if edge[3] in node[1]:
                n2 = node[1]
            if edge[4] in node[1]:
                n1 = node[1]

        if edge[0] == "OLINK":
            G.attr('edge', color="blue")
            G.edge(n1, n2, label=edge[2])
        if edge[0] == "QSLINK":
            G.attr('edge', color="red")
            G.edge(n1, n2, label=edge[2])

    # create legend
    with G.subgraph(name="cluster_0") as c:
        c.attr(style='filled', color='lightgrey', label="Legend", rankdir="TB")
        c.attr('node', style="filled", color="deepskyblue")
        c.node("Place", "Place")
        c.attr('node', style="filled", color="firebrick2")
        c.node("LOCATION", "LOCATION")
        c.attr('node', style="filled", color="lightgreen")
        c.node("SPATIAL_ENTITY", "SPATIAL_ENTITY")
        c.attr('node', style="filled", color="gold1")
        c.node("NONMOTION_EVENT", "NONMOTION_EVENT")
        c.attr('node', style="filled", color="hotpink")
        c.node("PATH", "PATH")
        c.attr('node', style="filled", color="blue", shape="rarrow")
        c.node("OLINK", "OLINK")
        c.attr('node', style="filled", color="red", shape="rarrow")
        c.node("QSLINK", "QSLINK")

        c.attr("edge", style="invis")
        c.edge("Place", "LOCATION")
        c.edge("LOCATION", "SPATIAL_ENTITY")
        c.edge("SPATIAL_ENTITY", "NONMOTION_EVENT")
        c.edge("NONMOTION_EVENT", "PATH")
        c.edge("PATH", "OLINK")
        c.edge("OLINK", "QSLINK")               
        
    G.view()

    
tree1 = ET.parse("C:/Users/leona/Downloads/training/Traning/RFC/Bicycles.xml")
tree2 = ET.parse("C:/Users/leona/Downloads/training/Traning/ANC/WhereToMadrid/Highlights_of_the_Prado_Museum.xml")

root1 = tree1.getroot()
root2 = tree2.getroot()

findNodesEdgesMetalinks(root1)
findNodesEdgesMetalinks(root2)
