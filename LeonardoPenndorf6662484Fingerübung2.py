import os
import spacy
import xml.etree.ElementTree as ET
from collections import Counter
from matplotlib import pyplot as plt
from tkinter import *
from tkinter import filedialog as fd


# Name: Leonardo Penndorf
# Matrikelnummer: 6662484


nlp = spacy.load("en_core_web_sm")


def openXml():
    root = Tk()
    root.withdraw()
    root.update()
    filePath = fd.askopenfilename()
    root.destroy()
    
    tree = ET.parse(filePath)
    root = tree.getroot()
    
    text = root[0].text

    a = linebreak(text)

    print("\n\n\n")

    b = tokenisierung(text)

    print("\n\n\n")

    c = countPOS(text)
    
    print("\n\n\n")

    d = countStuff(root)

    print("\n\n\n")
    
    e = countQsLink(root)

    print("\n\n\n")

    sentenceLength(text)
    
    print("\n\n\n")

    f = countSpatial(root)
    
    print("\n\n\n")

    g = countMotion(root)

    name = input("Wie soll die Text Datei heißen?")
    name += ".txt"
    stuff = open(name, "x")
    
    stuff.write(a)
    stuff.write("\n\n\n")    
    stuff.write(b)
    stuff.write("\n\n\n")
    stuff.write(c)
    stuff.write("\n\n\n")    
    stuff.write(d)
    stuff.write("\n\n\n")    
    stuff.write(e)
    stuff.write("\n\n\n")   
    stuff.write(f)
    stuff.write("\n\n\n")
    stuff.write(g)   


def linebreak(text):
    doc = nlp(text)
    strTxt = "Linebreak bei jeder Satzgrenze:\n"

    print("Linebreak bei jeder Satzgrenze:\n")
    for sent in doc.sents:
        print(sent.text)
        strTxt += sent.text

    return strTxt
        

def tokenisierung(text):
    doc = nlp(text)
    strToken = "Tokenisierung:\n"
    
    print("Tokenisierung:\n")
    for token in doc:
        print("Token: ", token.text, "Pos: ", token.pos_)
        strToken += "Token: " + token.text + "Pos: " + token.pos_ + "\n"

    return strToken


def countPOS(text):
    doc = nlp(text)
    strPos = "Wie oft kommen welche PoS-Tags vor?\n"
    
    print("Wie oft kommen welche PoS-Tags vor?\n")
    c = doc.count_by(spacy.attrs.POS)
    for k,v in sorted(c.items()):
        print(f'{k}. {doc.vocab[k].text:{8}}: {v}')
        strPos += f'{k}. {doc.vocab[k].text:{8}}: {v}' + "\n"

    return strPos


def countStuff(root):
    n = len(list(root[1]))
    i = 0
    spacialEntities = 0
    places = 0
    motions = 0
    locations = 0
    signals = 0
    qsLinks = 0
    oLinks = 0

    print("Wie viele [SpatialEntities, Places, Motions, Locations, Signals, QsLinks, OLinks] gibt es?\n")
    while i < n:
        tag = root[1][i].tag
        if tag == "SPATIAL_ENTITY":
            spacialEntities += 1
        if tag == "PLACE":
            places += 1
        if tag == "MOTION":
            motions += 1
        if tag == "LOCATION":
            locations += 1
        if tag == "SPATIAL_SIGNAL" or tag == "MOTION_SIGNAL":
            signals += 1
        if tag == "QSLINK":
            qsLinks += 1
        if tag == "OLINK":
            oLinks += 1
        i += 1

    print("SpacialEntities: ", spacialEntities)
    print("Places: ", places)
    print("Motions: ", motions)
    print("Locations: ", locations)
    print("Signals: ", signals)
    print("QSLinks: ", qsLinks)
    print("OLinks: ", oLinks)

    strStuff = "Wie viele [SpatialEntities, Places, Motions, Locations, Signals, QsLinks, OLinks] gibt es?\n"
    strStuff += "SpacialEntities: " + str(spacialEntities) + "\n"
    strStuff += "Places: " + str(places) + "\n"
    strStuff += "Motions: " + str(motions) + "\n"
    strStuff += "Locations: " + str(locations) + "\n"
    strStuff += "Signals: " + str(signals) + "\n"
    strStuff += "QSLinks: " + str(qsLinks) + "\n"
    strStuff += "OLinks: " + str(oLinks)

    return strStuff


def countQsLink(root):
    n = len(list(root[1]))
    i = 0

    #QsLinks realTypes
    IN = 0
    OUT = 0
    DC = 0
    EC = 0
    PO = 0
    TPP = 0
    ITPP = 0
    NTPP = 0
    INTPP = 0
    EQ = 0

    while i < n:
        if 'relType' in root[1][i].attrib:
            realType = root[1][i].attrib['relType']
            if realType == "IN":
                IN += 1
            if realType == "OUT":
                OUT += 1
            if realType == "DC":
                DC += 1
            if realType == "EC":
                EC += 1
            if realType == "PO":
                PO += 1
            if realType == "TPP":
                TPP += 1
            if realType == "ITPP":
                ITPP += 1
            if realType == "NTPP":
                NTPP += 1
            if realType == "INTPP":
                INTPP += 1
            if realType == "EQ":
                EQ += 1
        i += 1

    print("Wie oft kommen welche QsLink Typen vor?\n")
    print("IN: ", IN)
    print("OUT: ", OUT)
    print("DC: ", DC)
    print("EC: ", EC)
    print("PO: ", PO)
    print("TPP: ", TPP)
    print("ITPP: ", ITPP)
    print("NTPP: ", NTPP)
    print("INTPP: ", INTPP)
    print("EQ: ", EQ)

    strQs = "Wie oft kommen welche QsLink Typen vor?\n" + "\n"
    strQs += "IN: "  + str(IN) + "\n"
    strQs += "OUT: " + str(OUT) + "\n"
    strQs += "DC: " + str(DC) + "\n"
    strQs += "EC: " + str(EC) + "\n"
    strQs += "PO: " + str(PO) + "\n"
    strQs += "TPP: " + str(TPP) + "\n"
    strQs += "ITPP: " + str(ITPP) + "\n"
    strQs += "NTPP: " + str(NTPP) + "\n"
    strQs += "INTPP: " + str(INTPP)
    
    return strQs


def sentenceLength(text):
    doc = nlp(text)
    lengthList = []

    for sent in doc.sents:
        length = len(sent.text)
        if str(length) not in lengthList:
            lengthList.append(str(length))
            lengthList.append(1)

        else:
            lengthIndex = lengthList.index(str(length))
            lengthList[lengthIndex + 1] += 1


    i = 0
    xList = []
    yList = []
    while i <= (len(lengthList) - 1):
        xList.append(lengthList[i])
        yList.append(lengthList[i + 1])

        i += 2
    

    plt.bar(xList, yList)
    plt.title("Verteilung der Satzlänge graphisch darstellen")
    plt.xlabel("Satzlänge")
    plt.ylabel("Wie häufig?")
    name = input("Wie soll die Png Datei heißen?")
    name += ".png"
    plt.savefig(name)
    plt.show()


def countSpatial(root):
    n = len(list(root[1]))
    i = 0
    triggerList = [''] # ''
    QsList = []
    OList = []

    while i < n:
        tag = root[1][i].tag

        if tag == "SPATIAL_SIGNAL":
            triggerId = root[1][i].attrib['id']
            text = root[1][i].attrib['text']
            triggerList.append(triggerId)
            triggerList.append(text)

        i += 1
        
    i = 0

    while i < n:
        tag = root[1][i].tag
        
        if tag == "QSLINK":
            trigger = root[1][i].attrib['trigger']
            triggerIndex = triggerList.index(trigger)
            text = triggerList[triggerIndex + 1]

            if text not in QsList:
                QsList.append(text)
                QsList.append(1)

            else:
                QsIndex = QsList.index(text)
                QsList[QsIndex + 1] += 1

        if tag == "OLINK":
            trigger = root[1][i].attrib['trigger']
            triggerIndex = triggerList.index(trigger)
            text = triggerList[triggerIndex + 1]

            if text not in OList:
                OList.append(text)
                OList.append(1)

            else:
                OIndex = OList.index(text)
                OList[OIndex + 1] += 1
            
        i += 1

    strOutput = "Welche QsLinks werden von welchen Präpositionen getriggert?\n"
    print("Welche QsLinks werden von welchen Präpositionen getriggert?\n")
    
    i = 0

    while i < len(QsList):
        print(QsList[i], "triggered Qslinks", QsList[i +1], "mal")
        strOutput += QsList[i] + " triggered Qslinks " + str(QsList[i +1]) + " mal" +"\n"
        i += 2

    print("\n")
    strOutput += "\nWelche OLinks werden von welchen Präpositionen getriggert?\n"
    print("Welche OLinks werden von welchen Präpositionen getriggert?\n")

    i = 0

    while i < len(OList):
        print(OList[i], "triggered Olinks", OList[i +1], "mal")
        strOutput += OList[i] + " triggered Olinks " + str(OList[i +1]) + " mal" +"\n"
        i += 2
        
    return strOutput


def countMotion(root):
    n = len(list(root[1]))
    i = 0
    verbList = []

    while i < n:
        tag = root[1][i].tag
        if tag == "MOTION":
            verb = root[1][i].attrib['text']
            if verb not in verbList:
                verbList.append(verb)
                verbList.append(1)
                
            else:
                verbIndex = verbList.index(verb)
                verbList[verbIndex + 1] += 1
             
        i += 1


    strVerb = "Welches sind die fünf häufigsten „MOTION“ Verben?\n"
    print("Welches sind die fünf häufigsten „MOTION“ Verben?\n")
    i = 0

    while i <= 8:
        if i < len(verbList):
            print(verbList[i], "kommt", verbList[i + 1], "mal vor")
            strVerb += verbList[i] + " kommt " + str(verbList[i + 1]) + "mal vor" + "\n"
            
        i += 2
    
    return strVerb


openXml()
